#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import re, os, sys, time, zipfile, hashlib, arcpy
from ftplib import FTP
# this tracks a socket error for FTP, in case the server cannot get to bom FTP (firewall blocking etc)
import errno
from socket import error as socket_error
from os import path
import time
import datetime
import logging
import traceback

# Set the working folder paths...

WorkFolder = "\\\\Server04\\GIS_Data\\GIS_System\\Data_feeds\\BoM_Cyclones\\Shapes\\"
ArchiveFolder = "\\\\Server04\\GIS_Data\\GIS_System\\Data_feeds\\BoM_Cyclones\\Archive\\Archive\\" + str(time.strftime("%Y-%m-%d_%H%M")) + "\\"

def log(data):

    logfile = open("\\\\server04\\GIS_Data\\GIS_System\\Data_feeds\\BOM_Cyclones\BOM_Cyclone_ETL_Import_Tool.log", "a")
    logfile.write(str(time.strftime("%d/%m/%y %H:%M:%S - ")) + data + "\n")
    logfile.close()

# Define the download handler function    
def DownloadHandler(block):
    file.write(block)

print ("Starting...")
log("Starting FTP downloading process")

# Setup the FTP parameters...
# attempt to get the ftp files, note there is a facility to show and log problems getting access to ftp
# Note here that in development there is dummy data on the ftp directory sample anon/sample/IDBDM001/IDW60266
# The file available will be IDW60266.fix.zip
# when using this in production the ftp target directory is /anon/gen/fwo, and files will be there if there is a cyclone!
# note that on the ftp site the file names are different so change the single file list entry below from IDW60266.fix.zip to IDW60266.zip

try:
#     FILE_LIST = ["IDW60266.zip", "IDW60267.zip", "IDW60268.zip", "IDD65408.zip","IDD65401.zip", "IDD65402.zip", "IDQ65251.zip", "IDW60283.zip", "IDQ65248.zip", "IDQ65249.zip", "IDQ65250.zip", "IDQ65409.zip"]
    FILE_LIST = ["IDR02POL.200810132018-multipoint.zip", "IDR02POL.200810132024-multipoint.zip"]
#     ftp = FTP("ftp2.bom.gov.au")
    ftp = FTP("ftp2.bom.gov.au")
    ftp.login()
#     ftp.cwd("/anon/gen/fwo")
    ftp.cwd("/anon/sample/IDBRAPOL")
    print(FILE_LIST)
except socket_error as serr:
    log("FTP Failure")
    log(str(serr))
    print ("FTP error -------")
    print (str(serr))
for ZFILE in FILE_LIST:
    #------------------------------------------------------------------------------------
    # Create an empty file for the downloaded zip. Note that "wb" = write, binary format
    print(f"Opening {ZFILE}")
#     zfilename = ZFILE.rsplit("\\")[-1]
#     print(zfilename)
#     file = open(WorkFolder + zfilename, "wb")
    file = open(WorkFolder + ZFILE, "wb")

    # Download the file from FTP.
    try:
        ftp.retrbinary('RETR ' + ZFILE, DownloadHandler)
        file.close()
        log(ZFILE + " successfully downloaded")
        print (ZFILE + " sucessfully downloaded")
    except:
        file.close()
#         os.remove(WorkFolder + ZFILE)
        log("Error downloading " + ZFILE + " not found on BOM FTP")
        print (ZFILE + " not found on FTP")
        continue


    #------------------------------------------------------------------------------------

    # Extract all the files from archive (zip)
    try:
        zip_file = zipfile.ZipFile(WorkFolder + ZFILE, 'r')
        print (zip_file)
        for files in zip_file.namelist():  
            if files.endswith('/'):
                break
            data = zip_file.read(files)
            myfile_path = os.path.join(WorkFolder, files.split("/")[-1])
            myfile = open(myfile_path, "wb")
            myfile.write(data)
            myfile.close()
            print ("Extracted file - " + files)
            log("Extracted file - " + files)
    except:
        log("error with unzip process")
        continue
    zip_file.close()


    # Move zipfile to archive folder, this works just fine
    if not os.path.exists(ArchiveFolder):
        os.makedirs (ArchiveFolder)
    os.rename(WorkFolder + ZFILE, ArchiveFolder + ZFILE)
    # Remove the .gml file
    dotIndex = ZFILE.rfind('.')
    baseName = ZFILE[:dotIndex]
    #os.remove(WorkFolder + baseName + ".gml")

#------------------------------------------------------------------
# This section closes out the ftp session if one exists
try:
    ftp.quit()
except NameError as name_error:
    log("Cannot connect to FTP")
    log(str(name_error))


# In[ ]:




