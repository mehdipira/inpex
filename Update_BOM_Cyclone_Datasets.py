#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import re, os, sys, time, zipfile, hashlib, arcpy
import errno
from socket import error as socket_error
from os import path
import time
import datetime
import logging
import traceback

# Setting up the environment and log file
arcpy.env.workspace = "\\\\Server04\\GIS_Data\\GIS_System\\Data_feeds\\BoM_Cyclones\\Shapes\\"
prodSDE = "\\\\Server04\\GIS_Data\\GIS_Data\\sql.inpex.esriaustraliaonline.com.au.sde"
workingspace = arcpy.env.workspace

def log(data):

    logfile = open("\\\\server04\\GIS_Data\\GIS_System\\Data_feeds\\BOM_Cyclones\BOM_Cyclone_ETL_Import_Tool.log", "a")
    logfile.write(str(time.strftime("%d/%m/%y %H:%M:%S - ")) + data + "\n")
    logfile.close()

# Define the download handler function    
def DownloadHandler(block):
    file.write(block)

print ("Starting...")
log("Starting renaming process")

fcFiles = arcpy.ListFiles()
for fcfile in fcFiles:
    try:
        # Rename downloaded shapefiles - points, tracks and areas
        renamedFile = fcfile.rsplit('.')[0]
        if fcfile.find('fix.dbf') > 0:
            arcpy.management.Rename(fcfile, renamedFile + "_pts.dbf")
        elif fcfile.find('fix.shp') > 0:
            arcpy.management.Rename(fcfile, renamedFile + "_pts.shp")
        elif fcfile.find('fix.shx') > 0:
            arcpy.management.Rename(fcfile, renamedFile + "_pts.shx")
        elif fcfile.find('fix.prj') > 0:
            arcpy.management.Rename(fcfile, renamedFile + "_pts.prj")
        else:
            print("Already renamed!")
            log("Already renamed!")
        if fcfile.find('.areas.dbf') > 0:
            arcpy.management.Rename(fcfile, renamedFile + "_areas.dbf")
        elif fcfile.find('.areas.shp') > 0:
            arcpy.management.Rename(fcfile, renamedFile + "_areas.shp")
        elif fcfile.find('.areas.shx') > 0:
            arcpy.management.Rename(fcfile, renamedFile + "_areas.shx")
        elif fcfile.find('.areas.prj') > 0:
            arcpy.management.Rename(fcfile, renamedFile + "_areas.prj")
        else:
            print("Already renamed!")
            log("Already renamed!")
        if fcfile.find('.track.dbf') > 0:
            arcpy.management.Rename(fcfile, renamedFile + "_track.dbf")
        elif fcfile.find('.track.shp') > 0:
            arcpy.management.Rename(fcfile, renamedFile + "_track.shp")
        elif fcfile.find('.track.shx') > 0:
            arcpy.management.Rename(fcfile, renamedFile + "_track.shx")
        elif fcfile.find('.track.prj') > 0:
            arcpy.management.Rename(fcfile, renamedFile + "_track.prj")
        else:
            print("Already renamed!")
            log("Already renamed!")
        if fcfile.find('.windarea.dbf') > 0:
            arcpy.management.Rename(fcfile, renamedFile + "_windarea.dbf")
        elif fcfile.find('.windarea.shp') > 0:
            arcpy.management.Rename(fcfile, renamedFile + "_windarea.shp")
        elif fcfile.find('.windarea.shx') > 0:
            arcpy.management.Rename(fcfile, renamedFile + "_windarea.shx")
        elif fcfile.find('.windarea.prj') > 0:
            arcpy.management.Rename(fcfile, renamedFile + "_windarea.prj")
        else:
            print("Already renamed!")
            log("Already renamed!")
    except:
        log("error with feature renaming")
        continue
log("Renaming process completed")
print ("Renaming process completed")

log("Starting to add BOM ID field and update BOM cyclone datasets")
fcs = arcpy.ListFeatureClasses()
for fc in fcs:
    try:
        # Add the BOM ID field
        bomID = ''
        bomID = str(fc.split('_')[0])
        fields = arcpy.ListFields(fc)
        if "BOM_ID" not in fields:
            arcpy.AddField_management(fc, "BOM_ID", "TEXT", "", "", "16")
            arcpy.management.CalculateField(fc, "BOM_ID", 'bomID', "PYTHON3", '')
        if fc.find("_pts") > 0:
            arcpy.management.ConvertTimeField(fc, "fixTimeWA", "yyyy-MM-ddTHH:mm:ssZ", "DateWA", "DATE", "'Not Used'")
      
        else:
            print("This field already exits!")
            log("This field already exits!")
    except:
        pass
        log("error with calculating field")

    # Updating the BOM cyclone feature classes
    desc = arcpy.Describe(fc)
    if desc.shapeType == "Point" and fc.find("_pts.shp") > 0:
        arcpy.Append_management(fc, os.path.join(prodSDE, "geodata.GDO.ENVR_AUS_Meteorology_BOM_G94_cyclone_pts_1"), schema_type="NO_TEST")
    elif desc.shapeType == "Polyline" and fc.find("_track.shp") > 0:
        arcpy.Append_management(fc, os.path.join(prodSDE, "geodata.GDO.ENVR_AUS_Meteorology_BOM_G94_cyclone_track_1"), schema_type="NO_TEST")
    elif desc.shapeType == "Polygon" and fc.find("_areas.shp") > 0:
        arcpy.Append_management(fc, os.path.join(prodSDE, "geodata.GDO.ENVR_AUS_Meteorology_BOM_G94_cyclone_areas_1"), schema_type="NO_TEST")
#     elif desc.shapeType == "Polygon" and fc.find("_windarea.shp") > 0:
#         arcpy.Append_management(fc, os.path.join(prodSDE, "geodata.GDO.ENVR_AUS_Meteorology_BOM_G94_cyclone_windarea_1"), schema_type="NO_TEST")
    else:
        print("No more shapefiles to append!")
        log("No more shapefiles to append!")
log("BOM cyclone datasets updated")
print ("BOM cyclone datasets updated")

# Delete downloaded shapefiles
log("Deleting downloaded shapefiles")
# Delete downloaded shapefiles
fcs = arcpy.ListFeatureClasses()
for fc in fcs:
    try:        
        arcpy.Delete_management(fc)
        print("Specified shapefiles deleted!")
        
    except FileNotFoundError as noFile_error:
        log("Cannot find specified file!")
        log(str(noFile_error))      
log("Deleted downloaded shapefiles") 


# In[ ]:




